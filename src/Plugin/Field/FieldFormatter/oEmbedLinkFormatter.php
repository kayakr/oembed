<?php

/**
 * @file
 * Contains \Drupal\oembed\Plugin\Field\FieldFormatter\oEmbedLinkFormatter.
 */

namespace Drupal\oembed\Plugin\Field\FieldFormatter;

use Bangpound\oEmbed\Consumer;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'oembed_link' formatter.
 *
 * @FieldFormatter(
 *   id = "oembed_link",
 *   label = @Translation("oEmbed"),
 *   description = @Translation("Embeds links if possible - otherwise just links them."),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class oEmbedLinkFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Bangpound\oEmbed\Consumer
   */
  private $consumer;

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    Consumer $consumer
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->consumer = $consumer;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'maxwidth' => '',
      'maxheight' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['maxwidth'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Width'),
      '#default_value' => $this->getSetting('maxwidth'),
    ];
    $form['maxheight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Height'),
      '#default_value' => $this->getSetting('maxheight'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($maxwidth = $this->getSetting('maxwidth')) {
      $summary[] = $this->t(
        'Max Width: @maxwidth',
        array('@maxwidth' => $maxwidth)
      );
    }
    if ($maxheight = $this->getSetting('maxheight')) {
      $summary[] = $this->t(
        'Max Height: @maxheight',
        array('@maxheight' => $maxheight)
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /**
     * @var  $delta
     * @var \Drupal\link\Plugin\Field\FieldType\LinkItem $item
     */
    foreach ($items as $delta => $item) {
      $output = $this->consumer->get(
        $item->getUrl()->getUri(),
        $this->getSettings()
      );
      $elements[$delta] = [
        '#markup' => Markup::create((string) $output),
      ];
    }

    return $elements;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var \Bangpound\oEmbed\Consumer $consumer */
    $consumer = $container->get('oembed.consumer');

    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $consumer
    );
  }
}
