<?php

namespace Drupal\oembed;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RegisterOEmbedProviderClassesCompilerPass implements CompilerPassInterface {

  /**
   * @var string Service ID of the provider resolver
   */
  private $providerResolverService;

  /**
   * @var string Tag name for providers.
   */
  private $providerTag;

  /**
   * Constructor.
   *
   * @param string $providerResolverService Service name of the provider resolver in processed container
   * @param string $providerTag Tag name used for provider
   */
  public function __construct($providerResolverService = 'oembed.provider_resolver', $providerTag = 'oembed.provider')
  {
    $this->providerResolverService = $providerResolverService;
    $this->providerTag = $providerTag;
  }

  /**
   * You can modify the container here before it is dumped to PHP code.
   *
   * @param ContainerBuilder $container
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition($this->providerResolverService) && !$container->hasAlias($this->providerResolverService)) {
      return;
    }

    $definition = $container->findDefinition($this->providerResolverService);

    foreach ($container->findTaggedServiceIds($this->providerTag) as $id => $providers) {
      $definition->addMethodCall('addProvider', array(new Reference($id)));
    }
  }
}
