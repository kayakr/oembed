<?php

namespace Drupal\oembed\Element;

use Bangpound\oEmbed\Consumer;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\RenderElement;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class oEmbed
 * @package Drupal\oembed\Render\Element
 *
 * @RenderElement("oembed")
 */
class oEmbed extends RenderElement implements ContainerFactoryPluginInterface {

  protected $consumer;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, Consumer $consumer) {
    $this->consumer = $consumer;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Returns the element properties for this element.
   *
   * @return array
   *   An array of element properties. See
   *   \Drupal\Core\Render\ElementInfoManagerInterface::getInfo() for
   *   documentation of the standard properties of all elements, and the
   *   return value format.
   */
  public function getInfo() {
    $class = get_class($this);
    return array(
      '#theme' => 'oembed',
      '#embed' => NULL,
      '#parameters' => array(),
      '#attributes' => array(),
      '#pre_render' => array(
        array($class, 'preRenderFetch'),
        array($class, 'preRenderRetheme'),
      ),
    );
  }

  public static function preRenderFetch($element)
  {
    $embed = oembed_get_data($element['#url'], $element['#parameters']);

    // Prevent rendering if the response is bad.
    if (!$embed) {
      $element['#printed'] = TRUE;
      return $element;
    }

    $element['#embed'] = $embed;

    return $element;
  }

  public static function preRenderRetheme($element)
  {
    // Only act when the oEmbed response is true.
    if (!empty($element['#printed'])) {
      return $element;
    }

    /** @var \Bangpound\oEmbed\Response\Response $embed */
    $embed = $element['#embed'];
    $element['#theme'] = 'oembed__'. $embed->getType() .'__'. implode('__', explode(':', $embed->getProviderName(), 2));

    return $element;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Bangpound\oEmbed\Consumer $consumer */
    $consumer = $container->get('oembed.consumer');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $consumer
    );
  }
}