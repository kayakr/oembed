<?php

namespace Drupal\oembed\Element;

/**
 * Class oEmbedThumbnail
 * @package Drupal\oembed\Render\Element
 *
 * @RenderElement("oembed_thumbnail")
 */
class oEmbedThumbnail extends oEmbed {

  public function getInfo() {
    $class = get_class($this);
    return array(
      '#theme' => 'image',
      '#path' => NULL,
      '#width' => NULL,
      '#height' => NULL,
      '#alt' => '',
      '#title' => NULL,
      '#attributes' => array(),
      '#embed' => NULL,
      '#parameters' => array(),
      '#pre_render' => array(
        array($class, 'preRenderFetch'),
        array($class, 'preRenderThumbnail'),
      ),
    );
  }

  public static function preRenderThumbnail($element) {
    // Only act when the oEmbed response is true.
    if (!empty($element['#printed'])) {
      return $element;
    }

    /** @var \Bangpound\oEmbed\Response\Response $embed */
    $embed = $element['#embed'];

    // Check if the oEmbed response provides a thumbnail image.
    if (empty($embed->getThumbnailUrl())) {
      $element['#printed'] = TRUE;
      return $element;
    }

    self::oembed_pre_render_image_helper($element);

    return $element;
  }

  private static function oembed_pre_render_image_helper(&$element) {
    /** @var \Bangpound\oEmbed\Response\Response $embed */
    $embed = $element['#embed'];

    $element['#uri'] = $embed->getThumbnailUrl();
    $element['#alt'] = oembed_alt_attr($embed);
    $element['#title'] = $embed->getTitle();
    $element['#height'] = !empty($embed->getThumbnailHeight()) ? $embed->getThumbnailHeight() : NULL;
    $element['#width'] = !empty($embed->getThumbnailWidth()) ? $embed->getThumbnailWidth() : NULL;

    // theme_image() prefers width, height, alt and title element properties over
    // attributes so we manually override them if an associated attribute is set.
    foreach (array('width', 'height', 'alt', 'title') as $key) {
      if (isset($element['#attributes'][$key])) {
        $element['#' . $key] = $element['#attributes'][$key];
      }
    }
  }
}
