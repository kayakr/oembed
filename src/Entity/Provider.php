<?php

/**
 * @file
 * Contains \Drupal\oembed\Entity\Provider.
 */

namespace Drupal\oembed\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\oembed\ProviderInterface;

/**
 * Defines the Provider entity.
 *
 * @ConfigEntityType(
 *   id = "oembed_provider",
 *   label = @Translation("Provider"),
 *   handlers = {
 *     "list_builder" = "Drupal\oembed\ProviderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\oembed\Form\ProviderForm",
 *       "edit" = "Drupal\oembed\Form\ProviderForm",
 *       "delete" = "Drupal\oembed\Form\ProviderDeleteForm"
 *     }
 *   },
 *   config_prefix = "oembed_provider",
 *   admin_permission = "administer oembed",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/media/oembed/provider/{oembed_provider}",
 *     "edit-form" = "/admin/config/media/oembed/provider/{oembed_provider}/edit",
 *     "delete-form" = "/admin/config/media/oembed/provider/{oembed_provider}/delete",
 *     "collection" = "/admin/config/media/oembed/visibility_group"
 *   }
 * )
 */
class Provider extends ConfigEntityBase implements ProviderInterface {

  /**
   * The Provider ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Provider label.
   *
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $endpoint;

  /**
   * @var string
   */
  protected $scheme;

  public function getEndpoint() {
    return $this->endpoint;
  }

  public function getScheme() {
    return $this->scheme;
  }

  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  public function setScheme($scheme) {
    $this->scheme = $scheme;
  }

  /**
   * Adds cache contexts.
   *
   * @param string[] $cache_contexts
   *   The cache contexts to be added.
   *
   * @return $this
   */
  public function addCacheContexts(array $cache_contexts) {
    // TODO: Implement addCacheContexts() method.
  }

  /**
   * Adds cache tags.
   *
   * @param string[] $cache_tags
   *   The cache tags to be added.
   *
   * @return $this
   */
  public function addCacheTags(array $cache_tags) {
    // TODO: Implement addCacheTags() method.
  }

  /**
   * Merges the maximum age (in seconds) with the existing maximum age.
   *
   * The max age will be set to the given value if it is lower than the existing
   * value.
   *
   * @param int $max_age
   *   The max age to associate.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   *   Thrown if a non-integer value is supplied.
   */
  public function mergeCacheMaxAge($max_age) {
    // TODO: Implement mergeCacheMaxAge() method.
  }

  /**
   * Adds a dependency on an object: merges its cacheability metadata.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface|object $other_object
   *   The dependency. If the object implements CacheableDependencyInterface,
   *   then its cacheability metadata will be used. Otherwise, the passed in
   *   object must be assumed to be uncacheable, so max-age 0 is set.
   *
   * @return $this
   *
   * @see \Drupal\Core\Cache\CacheableMetadata::createFromObject()
   */
  public function addCacheableDependency($other_object) {
    // TODO: Implement addCacheableDependency() method.
  }
}
