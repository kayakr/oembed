<?php

/**
 * @file
 * Contains \Drupal\oembed\ProviderInterface.
 */

namespace Drupal\oembed;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Provider entities.
 */
interface ProviderInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.

  public function getEndpoint();

  public function getScheme();

  public function setEndpoint($endpoint);

  public function setScheme($scheme);
}
