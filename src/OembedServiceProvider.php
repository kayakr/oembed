<?php

namespace Drupal\oembed;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

class OembedServiceProvider implements ServiceProviderInterface {

  /**
   * Registers services to the container.
   *
   * @param ContainerBuilder $container
   *   The ContainerBuilder to register services to.
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new RegisterOEmbedProviderClassesCompilerPass());
  }
}
