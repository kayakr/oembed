<?php

namespace Drupal\oembed\StreamWrapper;

use Bangpound\oEmbed\Provider\ProviderInterface;
use Drupal\Core\StreamWrapper\ReadOnlyStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;

/**
 *  @file
 *  Create a oEmbed Stream Wrapper class.
 */
class oEmbedStream extends ReadOnlyStream {

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::READ_VISIBLE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return t('oEmbed resources');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('Resources provided by oEmbed.');
  }

  // As part of the inode protection mode returned by stat(), identifies the
  // file as a regular file, as opposed to a directory, symbolic link, or other
  // type of "file".
  // @see http://linux.die.net/man/2/stat

  const S_IFREG = 0100000;

  /**
   * Template for stat calls.
   *
   * All elements must be initialized.
   */
  protected $_stat = array(
    0 => 0, // Device number
    'dev' => 0,
    1 => 0, // Inode number
    'ino' => 0,
    // Inode protection mode. file_unmanaged_delete() requires is_file() to
    // return TRUE.
    2 => self::S_IFREG,
    'mode' => self::S_IFREG,
    3 => 0, // Number of links.
    'nlink' => 0,
    4 => 0, // Userid of owner.
    'uid' => 0,
    5 => 0, // Groupid of owner.
    'gid' => 0,
    6 => -1, // Device type, if inode device *
    'rdev' => -1,
    7 => 0, // Size in bytes.
    'size' => 0,
    8 => 0, // Time of last access (Unix timestamp).
    'atime' => 0,
    9 => 0, // Time of last modification (Unix timestamp).
    'mtime' => 0,
    10 => 0, // Time of last inode change (Unix timestamp).
    'ctime' => 0,
    11 => -1, // Blocksize of filesystem IO.
    'blksize' => -1,
    12 => -1, // Number of blocks allocated.
    'blocks' => -1,
  );

  /**
   * Returns a web accessible URL for the resource.
   *
   * This function should return a URL that can be embedded in a web page
   * and accessed from a browser. For example, the external URL of
   * "youtube://xIpLd0WQKCY" might be
   * "http://www.youtube.com/watch?v=xIpLd0WQKCY".
   *
   * @return string
   *   Returns a string containing a web accessible URL for the resource.
   */
  public function getExternalUrl() {
    return rawurldecode(substr($this->getUri(), 9));
  }

  /**
   * Base implementation of realpath().
   */
  public function realpath() {
    return $this->getExternalUrl();
  }

  /**
   * Base implementation of setUri().
   * @param string $uri
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Base implementation of getUri().
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Support for fopen(), file_get_contents(), file_put_contents() etc.
   *
   * @param string $uri
   *   A string containing the path to the file to open.
   * @param string $mode
   *   The file mode ("r", "wb" etc.).
   * @param int $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param string &$opened_url
   *   A string containing the path actually opened.
   *
   * @return bool
   *   TRUE if file was opened successfully.
   */
  public function stream_open($uri, $mode, $options, &$opened_url) {
    $this->setUri($uri);

    // We only handle Read-Only mode by default.
    if ($mode != 'r' && $mode != 'rb') {
      return FALSE;
    }

    $provider = \Drupal::service('oembed.provider.delegating')->supports($this->getExternalUrl());

    if ($provider === FALSE) {
      return FALSE;
    }

    if ((bool) $provider && ($options & STREAM_USE_PATH)) {
      $opened_url = $uri;
    }

    return (bool) $provider;
  }

  /**
   * Support for fread(), file_get_contents() etc.
   *
   * @param int $count
   *   Maximum number of bytes to be read.
   *
   * @return bool
   *   The string that was read, or FALSE in case of an error.
   */
  public function stream_read($count) {
    return FALSE;
  }

  /**
   * Support for feof().
   *
   * @return bool
   *   TRUE if end-of-file has been reached.
   */
  public function stream_eof() {
    return FALSE;
  }

  /**
   * Support for fseek().
   *
   * @todo document why this returns false.
   *
   * @param int $offset
   *   The byte offset to got to.
   * @param int $whence
   *   SEEK_SET, SEEK_CUR, or SEEK_END.
   * @return bool TRUE on success
   * TRUE on success
   */
  public function stream_seek($offset, $whence = SEEK_SET) {
    return FALSE;
  }

  /**
   * Support for ftell().
   *
   * @todo document why this returns false.
   *
   * @return bool
   *   The current offset in bytes from the beginning of file.
   */
  public function stream_tell() {
    return FALSE;
  }

  /**
   * Support for fstat().
   *
   * @return array
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   */
  public function stream_stat() {
    return $this->_stat;
  }

  /**
   * Support for fclose().
   *
   * @todo document why this returns TRUE.
   *
   * @return bool
   *   TRUE if stream was successfully closed.
   */
  public function stream_close() {
    return TRUE;
  }

  /**
   * Support for stat().
   *
   * @param string $url
   *   A string containing the url to get information about.
   * @param int $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return array
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   */
  public function url_stat($url, $flags) {
    return $this->stream_stat();
  }

  /**
   * Support for opendir().
   *
   * @param string $url
   *   A string containing the url to the directory to open.
   * @param int $options
   *   Whether or not to enforce safe_mode (0x04).
   *
   * @return bool
   *   TRUE on success.
   */
  public function dir_opendir($url, $options) {
    return FALSE;
  }

  /**
   * Support for readdir().
   *
   * @return bool
   *   The next filename, or FALSE if there are no more files in the directory.
   */
  public function dir_readdir() {
    return FALSE;
  }

  /**
   * Support for rewinddir().
   *
   * @return bool
   *   TRUE on success.
   */
  public function dir_rewinddir() {
    return FALSE;
  }

  /**
   * Support for closedir().
   *
   * @return bool
   *   TRUE on success.
   */
  public function dir_closedir() {
    return FALSE;
  }

  /**
   * Implements DrupalStreamWrapperInterface::dirname().
   * @param null $uri
   * @return bool|string
   */
  public function dirname($uri = NULL) {
    return FALSE;
  }

  /**
   * Retrieve the underlying stream resource.
   *
   * This method is called in response to stream_select().
   *
   * @param int $cast_as
   *   Can be STREAM_CAST_FOR_SELECT when stream_select() is calling
   *   stream_cast() or STREAM_CAST_AS_STREAM when stream_cast() is called for
   *   other uses.
   *
   * @return resource|false
   *   The underlying stream resource or FALSE if stream_select() is not
   *   supported.
   *
   * @see stream_select()
   * @see http://php.net/manual/streamwrapper.stream-cast.php
   */
  public function stream_cast($cast_as) {
    // TODO: Implement stream_cast() method.
  }

  /**
   * Change stream options.
   *
   * This method is called to set options on the stream.
   *
   * @param int $option
   *   One of:
   *   - STREAM_OPTION_BLOCKING: The method was called in response to
   *     stream_set_blocking().
   *   - STREAM_OPTION_READ_TIMEOUT: The method was called in response to
   *     stream_set_timeout().
   *   - STREAM_OPTION_WRITE_BUFFER: The method was called in response to
   *     stream_set_write_buffer().
   * @param int $arg1
   *   If option is:
   *   - STREAM_OPTION_BLOCKING: The requested blocking mode:
   *     - 1 means blocking.
   *     - 0 means not blocking.
   *   - STREAM_OPTION_READ_TIMEOUT: The timeout in seconds.
   *   - STREAM_OPTION_WRITE_BUFFER: The buffer mode, STREAM_BUFFER_NONE or
   *     STREAM_BUFFER_FULL.
   * @param int $arg2
   *   If option is:
   *   - STREAM_OPTION_BLOCKING: This option is not set.
   *   - STREAM_OPTION_READ_TIMEOUT: The timeout in microseconds.
   *   - STREAM_OPTION_WRITE_BUFFER: The requested buffer size.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise. If $option is not implemented, FALSE
   *   should be returned.
   */
  public function stream_set_option($option, $arg1, $arg2) {
    // TODO: Implement stream_set_option() method.
  }
}
