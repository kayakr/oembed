<?php

/**
 * @file
 * Contains \Drupal\oembed\Form\ProviderForm.
 */

namespace Drupal\oembed\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProviderForm.
 *
 * @package Drupal\oembed\Form
 */
class ProviderForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\oembed\ProviderInterface $provider */
    $provider = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $provider->label(),
      '#description' => $this->t("Label for the Provider."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $provider->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\oembed\Entity\Provider::load',
      ),
      '#disabled' => !$provider->isNew(),
    );

    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('The endpoint where oEmbed requests are going to be sent.'),
      '#size' => 32,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $provider->getEndpoint(),
    );

    $form['scheme'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Schemes'),
      '#description' => $this->t('Newline separated list of schemes like !example', array('!example' => 'http://*.revision3.com/*')),
      '#required' => TRUE,
      '#default_value' => implode(PHP_EOL, $provider->getScheme()),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate each scheme.
    $schemes = array_filter(array_map('trim', explode(PHP_EOL, $form_state->getValue('scheme'))));

    $form_state->setValue('scheme', $schemes);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $provider = $this->entity;
    $status = $provider->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Provider.', [
          '%label' => $provider->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Provider.', [
          '%label' => $provider->label(),
        ]));
    }
    $form_state->setRedirectUrl($provider->urlInfo('collection'));
  }

}
