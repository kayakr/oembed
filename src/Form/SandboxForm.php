<?php

namespace Drupal\oembed\Form;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

class SandboxForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'oembed_test';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('URL to request from oEmbed provider'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );

    if ($form_state->hasValue('embed')) {
      $renderer = \Drupal::service('renderer');

      $form['response'] = array(
        '#type' => 'container',
      );
      $form['response']['display']['oembed'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('oEmbed'),
      );

      $markup = $renderer->render($form_state->getValue(['display', 'oembed']));
      $form['response']['display']['oembed'][] = array(
        '#markup' => Markup::create($markup),
      );
      $form['response']['display']['oembed'][] = array(
        '#prefix' => '<pre>',
        '#markup' => SafeMarkup::checkPlain($markup),
        '#suffix' => '</pre>',
      );

      $form['response']['display']['oembed_thumbnail'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('oEmbed Thumbnail'),
      );

      $markup = $renderer->render($form_state->getValue(['display', 'oembed_thumbnail']));
      $form['response']['display']['oembed_thumbnail'][] = array(
        '#markup' => $markup,
      );
      $form['response']['display']['oembed_thumbnail'][] = array(
        '#prefix' => '<pre>',
        '#markup' => SafeMarkup::checkPlain($markup),
        '#suffix' => '</pre>',
      );
      $form['response']['details'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Details'),
      );
      $form['response']['details']['data'] = array(
        '#prefix' => '<pre>',
        '#markup' => $form_state->getValue('embed'),
        '#suffix' => '</pre>',
      );
    }
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Normalize input and look up
    $url = $form_state->getValue('url');
    $embed = oembed_get_data($url);
    if (!empty($embed)) {
      $form_state->setValue('embed', SafeMarkup::checkPlain(print_r($embed, TRUE)));
      $form_state->setValue(['display', 'oembed'], oembed_render_element('oembed', $url));
      $form_state->setValue(['display', 'oembed_thumbnail'], oembed_render_element('oembed_thumbnail', $url));
    }
    else {
      $form_state->setError($form['url'], $this->t(
        '%input is not valid oEmbed URL.',
        array(
          '%input' => $form_state->getValue('url'),
        )
      ));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }
}