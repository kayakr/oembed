<?php

/**
 * @file
 * Contains \Drupal\oembed\Form\SettingsForm.
 */

namespace Drupal\oembed\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['oembed.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'oembed_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oembed.settings');

    $periods = array(
      3600, 10800, 21600, 32400, 43200, 86400, 172800,
      259200, 604800, 1209600, 2419200, 4838400, 9676800
    );
    $period = array_map(
      array(\Drupal::service('date.formatter'), 'formatInterval'),
      array_combine($periods, $periods)
    );
    $period[CacheBackendInterface::CACHE_PERMANENT] = t('Indefinite');
    $form['cache'] = array(
      '#type' => 'details',
      '#title' => $this->t('Cache'),
      '#open' => TRUE,
      '#tree' => TRUE,
    );
    $form['cache']['lifetime'] = array(
      '#type' => 'select',
      '#title' => $this->t('Minimum oEmbed cache lifetime'),
      '#options' => $period,
      '#default_value' => $config->get('cache.lifetime'),
      '#description' => $this->t('Cached oEmbed output will not be re-requested until at least this much time has elapsed.'),
    );
    $form['cache']['flush'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Clear oEmbed cache when all Drupal caches are cleared'),
      '#default_value' => $config->get('cache.flush'),
      '#description' => $this->t('Unselect this to retain unexpired cached oEmbed output even when drupal_flush_all_caches() is called. In conjunction with a long %lifetime, this can help reduce costs when using an oEmbed provider service that charges a fee per request.', array('%lifetime' => t('Minimum oEmbed cache lifetime'))),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('oembed.settings')
      ->set('cache.lifetime', $form_state->getValue(['cache','lifetime']))
      ->set('cache.flush', $form_state->getValue(['cache','flush']))
      ->save();

  }
}
