<?php

/**
 * @file
 * Input filter that enhances oEmbed enabled URLs with extra content
 */

define('OEMBED_PATTERN_AUTOEMBED', '|^\s*(https?://[^\s"]+)\s*$|im');
define('OEMBED_PATTERN_EMBED_SHORTCODE', '/(.?)\[embed\b(.*?)\](.+?)\[\/embed\](.?)/s');
define('OEMBED_PATTERN_EMBED_UNWRAP', '/<p>\s*+(\[embed\b.*?\].+?\[\/embed\])\s*+<\/p>/s');

/**
 * Implements hook_filter_info().
 */
function oembed_filter_info() {
  $filters['oembed_legacy'] = array(
    'title' => t('oEmbed legacy filter'),
    'description' => t('Embeds content for oEmbed-enabled web addresses and turns the rest, and e-mail addresses, into clickable links.'),
    'process callback' => 'oembed_filter_oembed_legacy_process',
    'settings callback' => 'oembed_filter_oembed_legacy_settings',
    'default settings' => array(
      'maxwidth' => '',
      'maxheight' => '',
    ),
  );
  return $filters;
}

/**
 * Implements hook_filter_FILTER_settings().
 */
function oembed_filter_oembed_legacy_settings($form, &$form_state, $filter, $format, $defaults, $filters) {
  module_load_include('inc', 'oembed', 'oembed_legacy');
  return _oembed_filter_settings($form, $form_state, $filter, $format, $defaults);
}

/**
 * Implements hook_filter_FILTER_process().
 */
function oembed_filter_oembed_legacy_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  module_load_include('inc', 'oembed', 'oembed_legacy');
  return _oembed_filter_apply($text, $filter, $format, $langcode, $cache, $cache_id);
}

/**
 * Extract all URLs for oEmbed to process.
 *
 * Returns an array of URLs grouped by field, delta, and column.
 */
function _oembed_field_extract_urls($entity_type, $entity) {
  $urls = array();

  // Determine if any formats use oEmbed filter.
  $filter_settings = array();
  foreach (filter_formats() as $format) {
    $filters = filter_list_format($format->format);
    if (isset($filters['oembed']) && $filters['oembed']->status) {
      $filter_settings[$format->format] = $filters['oembed']->settings;
    }
  }

  if (!empty($filter_settings)) {

    list(, , $bundle) = entity_extract_ids($entity_type, $entity);
    $instances = field_info_instances($entity_type, $bundle);

    foreach ($instances as $info) {

      // All text fields have a text_processing setting. Only search text fields with
      // text processing enabled.
      if (isset($info['settings']['text_processing']) && $info['settings']['text_processing']) {
        $items = field_get_items($entity_type, $entity, $info['field_name']);
        if (!$items) {
          continue;
        }
        foreach ($items as $delta => $item) {
          if (isset($filter_settings[$item['format']])) {

            // URLs may be contained within the other column values.
            foreach (array('value', 'summary') as $column) {
              if (!empty($item[$column])) {
                $text = $item[$column];

                // copied from oembed_filter_oembed_prepare().
                if ($filter_settings[$item['format']]['autoembed']) {
                  $text = preg_replace_callback(OEMBED_PATTERN_AUTOEMBED, 'oembed_preg_auto_replace', $text);
                }

                // copied from oembed_filter_oembed_process().
                $matches = array();
                preg_match_all(OEMBED_PATTERN_EMBED_SHORTCODE, $text, $matches);
                $urls[$info['field_name']][$delta][$column] = array_filter($matches[3], '_oembed_field_filter_urls');
              }
            }
          }
        }
      }
    }
  }

  return $urls;
}

/**
 * array_filter() callback that removes URLs for which there is no provider.
 */
function _oembed_field_filter_urls($match) {
  $matches = array();
  if (oembed_get_provider($match, $matches)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_attach_validate().
 */
function oembed_field_attach_validate($entity_type, $entity, array &$errors) {
  foreach (_oembed_field_extract_urls($entity_type, $entity) as $field_name => $items) {
    foreach ($items as $delta => $item) {
      foreach ($item as $column => $urls) {
        $messages = array();
        foreach ($urls as $url) {
          $embed = oembed_get_data($url);
          $validation_errors = oembed_validate_response($embed);
          if (!empty($validation_errors)) {
            $message = t('!url could not be embedded.', array('!url' => l(_filter_url_trim($url, 50), $url)));
            $message .= theme('item_list', array('items' => $validation_errors));
            $messages[] = $message;
          }
        }
        if (!empty($messages)) {
          $errors[$field_name][$entity->language][$delta][] = array(
            'error' => 'oembed_'. $column,
            'message' => theme('item_list', array('items' => $messages)),
            'repeat' => TRUE,
          );
        }
      }
    }
  }
}
